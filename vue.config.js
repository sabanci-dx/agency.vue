// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJson = require('./package.json');
process.env.VUE_APP_VERSION = packageJson.version;
process.env.VUE_APP_VUE_DEPS = JSON.stringify(packageJson.dependencies);
process.env.VUE_APP_VUE_VERSION = packageJson.dependencies.vue;

let scssVariable = '';
for (let envKey in process.env) {
  if (/VUE_APP_/i.test(envKey)) {
    if (envKey !== 'VUE_APP_VUE_DEPS') {
      scssVariable += `$${envKey}: "${process.env[envKey]}";`;
    }
    // console.log('########', `$${envKey}: "${process.env[envKey]}";`)
  }
}

module.exports = {
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: `
        ${scssVariable}
        `,
      },
    },
  },
  configureWebpack: {
    devtool: 'source-map',
  },
};
