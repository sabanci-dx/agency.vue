import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName "home" */ '@/views/home.page.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@/components/not-found/not-found.component.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  linkActiveClass: 'active',
  linkExactActiveClass: 'sub-active',
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition ? savedPosition : to.hash ? { el: to.hash, behavior: 'smooth' } : { top: 0, behavior: 'smooth' };
  },
});

export default router;
