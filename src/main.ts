import { createApp } from 'vue';

import router from './router';
import store from './store';
import { filters } from './filters';
import App from './App.vue';

const app = createApp(App);

// Auto Register Components Globally
const requireComponent = require.context('./components', true, /.*\.(vue|js)$/);
requireComponent.keys().forEach(function (fileName) {
  let baseComponentConfig = requireComponent(fileName);
  baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
  const baseComponentName = baseComponentConfig.name || fileName.replace(/^.+\//, '').replace(/\.\w+$/, '');
  app.component(baseComponentName, baseComponentConfig);
});

// Filters (or another name call Pipes)
app.config.globalProperties.$filters = { ...filters };

app.use(store).use(router).mount('body');
