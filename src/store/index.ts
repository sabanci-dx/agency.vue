import { createStore } from 'vuex';

// export interface Store {}

export default createStore({
  state: {
    // state = $data
  },
  getters: {
    // getters = Computed value (state: Store, getters)
    // this.$store.getters.getProducts
  },
  actions: {
    // fetch
  },
  mutations: {
    // setters = Methods(state, value)
    // this.$store.commit('addProduct', {name: 'kabak', count: 21})
  },
});
