## Notes:
 - Theme, Font, Global scss'ler için assets/scss altındaki scss dosyalarımızı kullanıyoruz. Bunları App.vue içerisinde importer.scss ile projeye aktarıyoruz
 - Bileşenlerimiz, Filtrelerimiz, Directive ve Plugin'lerimiz için kendi klasörlerini kullanıyoruz. İsimlendirme standardımız: *.component.vue, *.directive.vue, *.filter.vue şeklindedir. İsimde birden çok kelime içeriyorsa araya "-" ekleyerek ilerliyoruz (ornek-dosya-adi.component.vue).
 - Routing için Route klasörü altındaki index.ts üzerinde gerekli tanımlamaları yapabilirsiniz.
 - State management için Vuex kullanıyoruz, Store klasörü altında index.ts içerisinde bulabilirsiniz.
 - View klasörü altında görüntülenecek sayfalarımızı tutuyoruz. Dosyaları isimlendirirken home.page.vue, about.page.vue gibi *.page.vue standartını benimsiyoruz.
 - Runtime ve Compile time console loglarında warning veya error olmamasını önemsiyoruz.
 - Commit'ler konusunda [Git Commit Message Convention](https://dev.to/i5han3/git-commit-message-convention-that-you-can-follow-1709) standartlarına dikkat edilmesine dikkat ediyoruz

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
